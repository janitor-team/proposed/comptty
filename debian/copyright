Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: comptty
Source: www.w1hkj.com/files/test_suite/


Files: *
Copyright: (C) 2014-2016 Dave Freese <iam_w1hkj@w1hkj.com>
License: GPL-2+

Files: debian/*
Copyright: (C) 2016 Ana Custura <ana@netstat.org.uk>
License: GPL-2+

Files: data/win32/fl_app.nsi
Copyright: (C) 2009 Stelios Bounanos <m0gld@enotty.net>, Dave Freese <iam_w1hkj.com>
License: ZLIB

Files: build-aux/install-sh
Copyright: (C) 1994 X Consortium
License: X11

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, a copy of the full license text is available in
 /usr/share/common-licenses/GPL-2.

License: X11
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice appear in all copies and that both that copyright
 notice and this permission notice appear in supporting documentation, and
 that the name of the copyright holders not be used in advertising or
 publicity pertaining to distribution of the software without specific,
 written prior permission.  The copyright holders make no representations
 about the suitability of this software for any purpose.  It is provided "as
 is" without express or implied warranty.
 .
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 OF THIS SOFTWARE.

License: ZLIB
 This software is provided 'as-is', without any express or implied warranty. In
 no event will the authors be held liable for any damages arising from the use
 of this software.
 .
 Permission is granted to anyone to use this software for any purpose, including
 commercial applications, and to alter it and redistribute it freely, subject to
 the following restrictions:
 .
 The origin of this software must not be misrepresented; you must not claim
 that you wrote the original software. If you use this software in a product, an
 acknowledgment in the product documentation would be appreciated but is not
 required.  Altered source versions must be plainly marked as such, and must not
 be misrepresented as being the original software.  This notice may not be
 removed or altered from any source distribution.
